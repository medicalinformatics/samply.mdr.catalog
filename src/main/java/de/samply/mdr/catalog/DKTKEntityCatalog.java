/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.catalog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import de.samply.config.util.JAXBUtil;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.Code.SubCode;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.Definitions;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.mdr.xsd.Slot;
import de.samply.mdr.xsd.Slots;
import de.samply.share.model.upload.attribute.Attribute;
import de.samply.share.model.upload.query.And;
import de.samply.share.model.upload.query.Eq;
import de.samply.share.model.upload.query.Like;
import de.samply.share.model.upload.query.Or;
import de.samply.string.util.StringUtil;

/**
 *
 */
public class DKTKEntityCatalog {

    public static void main(String[] args) throws FileNotFoundException, IOException, JAXBException {
        File input = new File("src/main/csv/entities.csv");

        Catalog catalog = new Catalog();

        Definition definition = new Definition();
        definition.setLang("de");
        definition.setDesignation("Entitätenkatalog im DKTK");
        definition.setDefinition("Der Entitätenkatalog im DKTK");

        catalog.setDefinitions(new Definitions());
        catalog.getDefinitions().getDefinition().add(definition);

        Reader in = new InputStreamReader(new FileInputStream(input), StandardCharsets.UTF_8);
        CSVParser parser = new CSVParser(in, CSVFormat.EXCEL.withDelimiter(';'));
        Iterable<CSVRecord> records = parser.getRecords();
        parser.close();

        Code superCode = null;
        Code code = null;

        int number = 1;
        int order = 1;

        Or orIcd = null;
        Or orIcdo3 = null;

        for(CSVRecord record : records) {
            String catalogName = record.get(0);
            String group = record.get(1);
            String icd10 = record.get(2);
            String icdo3 = record.get(4);

            if(!StringUtil.isEmpty(catalogName)) {
                superCode = new Code();
                superCode.setDefinitions(new Definitions());
                superCode.setIsValid(false);
                superCode.setCode("" + (number++));
                superCode.setOrder(order++);

                Definition def = new Definition();
                def.setLang("de");
                def.setDefinition(catalogName);
                def.setDesignation(catalogName);

                superCode.getDefinitions().getDefinition().add(def);

                catalog.getCode().add(superCode);
            }

            if(!StringUtil.isEmpty(group)) {
                if(code != null) {
                    And a = new And();

                    if(orIcd.getAndOrEqOrLike().size() == 1) {
                        a.getAndOrEqOrLike().add(orIcd.getAndOrEqOrLike().get(0));
                    } else {
                        a.getAndOrEqOrLike().add(orIcd);
                    }

                    if(orIcdo3.getAndOrEqOrLike().size() == 1) {
                        a.getAndOrEqOrLike().add(orIcdo3.getAndOrEqOrLike().get(0));
                    } else {
                        a.getAndOrEqOrLike().add(orIcdo3);
                    }

                    Slot s = new Slot();
                    s.setKey("DKTK_SEARCH");
                    s.setValue(JAXBUtil.marshall(a, JAXBContext.newInstance(And.class, Or.class)));

                    code.getSlots().getSlot().add(s);
                }

                code = new Code();
                code.setDefinitions(new Definitions());
                code.setSlots(new Slots());
                code.setIsValid(true);
                code.setCode("" + (number++));

                Definition def = new Definition();
                def.setLang("de");
                def.setDefinition(group);
                def.setDesignation(group);

                code.getDefinitions().getDefinition().add(def);

                SubCode sub = new SubCode();
                sub.setCode(code.getCode());
                superCode.getSubCode().add(sub);

                catalog.getCode().add(code);

                orIcd = new Or();
                orIcdo3 = new Or();
            }


            if(!StringUtil.isEmpty(icd10)) {
                Like l = new Like();
                Attribute attr = new Attribute();
                attr.setMdrKey("urn:dktk:dataelement:29:2");
                attr.setValue(icd10.replace('*', '%'));
                l.setAttribute(attr);

                orIcd.getAndOrEqOrLike().add(l);
            }

            if(!StringUtil.isEmpty(icdo3)) {
                Eq l = new Eq();
                Attribute attr = new Attribute();
                attr.setMdrKey("urn:dktk:dataelement:7:2");
                attr.setValue(icdo3.replace('*', '%'));
                l.setAttribute(attr);

                orIcdo3.getAndOrEqOrLike().add(l);
            }
        }

        And a = new And();
        if(orIcd.getAndOrEqOrLike().size() == 1) {
            a.getAndOrEqOrLike().add(orIcd.getAndOrEqOrLike().get(0));
        } else {
            a.getAndOrEqOrLike().add(orIcd);
        }

        if(orIcdo3.getAndOrEqOrLike().size() == 1) {
            a.getAndOrEqOrLike().add(orIcdo3.getAndOrEqOrLike().get(0));
        } else {
            a.getAndOrEqOrLike().add(orIcdo3);
        }

        Slot s = new Slot();
        s.setKey("DKTK_SEARCH");
        s.setValue(JAXBUtil.marshall(a, JAXBContext.newInstance(And.class, Or.class)));
        code.getSlots().getSlot().add(s);


        Writer writer = new OutputStreamWriter(new FileOutputStream("output.xml"), StandardCharsets.UTF_8);
        writer.write(JAXBUtil.marshall(
                new ObjectFactory().createCatalog(catalog),
                JAXBContext.newInstance(ObjectFactory.class)));

        writer.close();
    }

}
