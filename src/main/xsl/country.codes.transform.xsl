<?xml version="1.0" encoding="UTF-8" ?>
<!--

    Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
    Contact: info@osse-register.de

    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU Affero General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your option) any
    later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU Affero General Public License
    along with this program; if not, see <http://www.gnu.org/licenses>.

    Additional permission under GNU GPL version 3 section 7:

    If you modify this Program, or any covered work, by linking or combining it
    with Jersey (https://jersey.java.net) (or a modified version of that
    library), containing parts covered by the terms of the General Public
    License, version 2.0, the licensors of this Program grant you additional
    permission to convey the resulting work.

-->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:mdr="http://mdr.samply.de" version="2.0"
    xmlns="http://schema.samply.de/mdr/common">

    <xsl:output method="xml" omit-xml-declaration="no" byte-order-mark="no" encoding="UTF-8" indent="yes" />

    <xsl:template match="/">
        <xsl:element name="catalog">
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2</xsl:element>
                    <xsl:element name="definition">ISO 3166-1 Alpha 2 ist ein Standard für die zweistellige Kodierung von Staaten, herausgegeben von der Internationalen Organisation für Normung (ISO).</xsl:element>
                </xsl:element>

                <xsl:element name="definition">
                    <xsl:attribute name="lang">en</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2</xsl:element>
                    <xsl:element name="definition">ISO 3166-1 Alpha 2 is a standard for two letter country codes, released by the International Organization for Standardization.</xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:call-template name="continent">
                <xsl:with-param name="region-code">150</xsl:with-param>
                <xsl:with-param name="name-de">Europa</xsl:with-param>
                <xsl:with-param name="name-en">Europe</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="region-code">002</xsl:with-param>
                <xsl:with-param name="name-de">Afrika</xsl:with-param>
                <xsl:with-param name="name-en">Africa</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="region-code">142</xsl:with-param>
                <xsl:with-param name="name-de">Asien</xsl:with-param>
                <xsl:with-param name="name-en">Asia</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="region-code">019</xsl:with-param>
                <xsl:with-param name="name-de">Amerika</xsl:with-param>
                <xsl:with-param name="name-en">America</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="region-code">009</xsl:with-param>
                <xsl:with-param name="name-de">Australien</xsl:with-param>
                <xsl:with-param name="name-en">Australia</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="region-code">NULL</xsl:with-param>
                <xsl:with-param name="name-de">Andere</xsl:with-param>
                <xsl:with-param name="name-en">Other</xsl:with-param>
            </xsl:call-template>

            <xsl:for-each select="//country">
                <xsl:element name="code">
                    <xsl:attribute name="code">
                        <xsl:value-of select="@alpha-2"/>
                    </xsl:attribute>
                    <xsl:attribute name="isValid">true</xsl:attribute>

                    <xsl:element name="definitions">
                        <xsl:element name="definition">
                            <xsl:attribute name="lang">en</xsl:attribute>
                            <xsl:element name="designation">
                                <xsl:value-of select="@name" />
                            </xsl:element>
                            <xsl:element name="definition">
                                <xsl:value-of select="@alpha-2" />
                                <xsl:value-of select="', '" />
                                <xsl:value-of select="@name" />
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template name="continent">
        <xsl:param name="region-code"/>
        <xsl:param name="name-de"/>
        <xsl:param name="name-en"/>

        <xsl:element name="code">
            <xsl:attribute name="code">
                <xsl:value-of select="$region-code"/>
            </xsl:attribute>
            <xsl:attribute name="isValid">false</xsl:attribute>
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="$name-de" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'Die Länder in '"/>
                        <xsl:value-of select="$name-de" />
                    </xsl:element>
                </xsl:element>
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="$name-en" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'The countries in '"/>
                        <xsl:value-of select="$name-en" />
                    </xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:if test="$region-code != 'NULL'">
                <xsl:for-each select="//country[@region-code=$region-code]">
                    <xsl:element name="subCode">
                        <xsl:attribute name="code" select="@alpha-2"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="$region-code = 'NULL'">
                <xsl:for-each select="//country[not(@region-code)]">
                    <xsl:element name="subCode">
                        <xsl:attribute name="code" select="@alpha-2"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:if>
        </xsl:element>
    </xsl:template>
</xsl:transform>

